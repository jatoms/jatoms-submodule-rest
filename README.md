# README
This is a basic Karaf Rest submodule template meant to be used with the [Jatoms Maven Plugin](https://gitlab.com/jatoms/jatoms-maven-plugin).
It contains 
* a simple rest endpoint (ItemEndpoint)
* the needed setup for OpenAPI documentation
* a frontend resource for the OpenAPI documentation
* an application (API) that serves as a common root
as well as all needed dependencies and a mixinfeature.xml for a simple feature.

## Quickstart
This template is meant for usage with the jatoms-maven-plugin.

In your project folder where you want to add this template as a subproject type
`mvn jatoms:submodule -Djrepo=https://gitlab.com/jatoms/jatoms-submodule-rest -Djcoords=<your groupId>:<your artifactId>:<your version>`

After the template has been processed you can build it via `mvn clean install` and then install the newly added feature into your karaf instance, via
`feature:install simple-rest-endpoint`.

The documentation for your rest endpoint should be reachable afterwards under `localhost:8181/doc`, whereas your endpoint should be reachable under `localhost:8181/api/item`