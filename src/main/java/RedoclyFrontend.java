package ${JATOMS.package};

import org.osgi.service.component.annotations.Component;
import org.osgi.service.http.whiteboard.propertytypes.HttpWhiteboardResource;

/**
 * Registers a resource that serves the index.html from the resources folder
 */
@Component(service = RedoclyFrontend.class)
@HttpWhiteboardResource(pattern = "/doc/*", prefix = "/index.html")
public class RedoclyFrontend {}